## BBC Take Home Exercise

I have chosen to prepare, analyse, and produce a machine learning pipeline
for category prediction of newspaper articles from Dataset: BBC. 

## Folders

In this repository you will find the following folders and their content:

### Data

Contains the raw text files in the `bbc` directory with subfolders for `business`, `entertainment`, `politics`, `sport` and `tech`, as well as the aggregated text data with applied labels `TextData.csv` and the same data in pickle form `DF.pickle`. The `Pickles` directory contains presliced version of the data for (`X_test`, `X_train`, `Y_test`, `Y_train`). 

### PickledModels

Contains pickled models for the ML methods employed for classification. These pickles are useful for testing without rebuilding the model.

### PreProcess

The scripts in this directory show how I scraped the text content of each file and created a cumulative dataset of the combined data for feeding into the ML models. 

### src

Inside this directory are subdirectories for the steps I took to explore and analyze this data.

#### EDA

Exploratory data analysis was performed on the data to try to identify high-level features that were distinguishing for specific categories. Up to this point, the data has not been altered in any way. I also decided to quickly test if dimensionality reduction was a useful tool for this data.

#### Feature Engineering 

Single file which shows the conversion of the raw text to cleaned and filtered text strings with less noise.

#### ML Models

Here I have placed a file for each of the methods tested for text classification.

**Randomforest**

Random Forest is a consensus testing algorithm for classification and regression. It is already used [citation 1] for text classification purposes, and has been shown to work even in imbalanced sets [citation 2]. However, it might be slightly less successful (20% reduced accuracy) for shorter texts meaning it is possibly only appropriate for longer texts [citation 3], though the precise cut off for length is poorly defined. There are a number of hyperparameters that may be tuned to optimize the model. The number of trees, the number of samples required to make splits in those trees, the number of trees sampled and even the number of samples required to create a leaf are all changeable values so this model supplies plenty of opportunity for fine tuning. The code for model building and hyperparameter tuning are all available in `/src/MLmodels/Random_forest.ipynb`.

[citation 1](https://www.hindawi.com/journals/jam/2014/425731/abs/)
[citation 2](https://www.sciencedirect.com/science/article/abs/pii/S0950705114002251)
[citation 3](https://link.springer.com/chapter/10.1007/978-3-319-10160-6_26)


**Support Vector Machines**

As with Random Forest, SVM is a machine learning algorithm for regression and classification but instead of making an ensemble of results of multiple decision trees, it defines a hyperplane that divides classes. For the purpose of text categorisation, they are reasonably well explored [citation 4], and show promise for their accuracy, speed of training and evaluation [citation 5]. They are also a good candidate for hyperparameter tuning to create a better fit model. SV models separate classes in higher order space and can be tuned to heavily penalise incorrect classifications, which makes it very useful for imbalanced data. The boundaries this algorithm draws are not necessarily straight lines, making it ideal for multi-class classification. The code for model building and hyperparameter tuning are all available in `/src/MLmodels/SVM.ipynb`.

[citation 4](https://link.springer.com/chapter/10.1007/BFb0026683)
[citation 5](https://www.microsoft.com/en-us/research/publication/inductive-learning-algorithms-and-representations-for-text-categorization/)

**Gradient Bagging**

Gradient boosting is another example of ensemble learning, like random forest, that uses many weaker models to create a stronger prediction with optimization via a differentiable loss function. The literature of GB for text classification is less extensive, but it has been found to be sensitive enough to successfully predict author gender from a corpus of de-sexed gender neutral books [citation 6]. The fact that the loss function is differentiable means that the loss can be easily optimized to improve performance of the model. Differentiable loss functions, like gradient descent, are also used to measure and optimize performance of other models like neural networks. The code for model building and hyperparameter tuning are all available in `/src/MLmodels/GBM.ipynb`.

[citation 6](https://www.sciencedirect.com/science/article/pii/S1877050916326849)

**A multinomial Bayesian method**

Bayesian approaches to text classifications are well studied [citation 7] primarily because of their computational efficiency (relative to other models) and solid performance [^citation 8]. These models have been used commercially for spam detection, they work by assigning class probabilities for each word and defining a conditional probability for each class on the sum of the words in each corpus. The code for model building are all available in `/src/MLmodels/MultinomialBayes.ipynb`. 

[citation 7](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.231.7128&rep=rep1&type=pdf)
[citation 8](https://link.springer.com/chapter/10.1007/11871637_49)

**Neural Network Approach**

Lastly, a neural network was constructed and trained to predict the labels in test data. NNs have extensive application in NLP [citation 9] for text processing, labeling and categorisation [citation 10]. DL methods are often the performance benchmark for testing new technologies [citation 11] and have been shown to outperform most statistical or machine learning techniques in this regard [citation 12]. The code for model building and hyperparameter tuning are all available in `/src/NN/n2.ipynb`.

[citation 9](https://www.aaai.org/ocs/index.php/AAAI/AAAI15/paper/viewPaper/9745)
[citation 10](https://arxiv.org/abs/1511.08630)
[citation 11](https://arxiv.org/abs/1607.01759)
[citation 12](https://link.springer.com/chapter/10.1007/978-3-662-44851-9_28)


## Results

EDA revealed there was no significant difference in the number of articles in each category, which is good from a classification perspective as imbalance would have required bootstrapping or another statistical technique to correct for. 

![Barplot of the number of each article type](Data/figs/barplot.png)

Analysis of the average length of the text revealed significant differences between every pair of article length means, except for entertainment and sport. However, classification by mean article length would inevitably result in a large number of incorrect classifications, as length inherently makes no statement as to the content of the article.

![Distributions of the length of articles by label](Data/figs/dist.png)

Word clouds were created to visualise if there were significant differences in the most used words per category. Even with stop words removed, the overlap is visually large. Word analysis will be revisited with unigrams later. 

![word clouds](Data/figs/wc.png)

Exploration of the average reading level of the text via SMOG index analysis revealed a distinct difference in the reading level of sports compared to the other articles.

| Category        | SMOG reading level           |
| ------------- |:-------------:|
| Business      | 13 |
| Entertainment      | 13      |
| Politics | 13      |
| Sports      | 10      |
| Tech | 12      |


Next, I looked at the variance present in the dataset using PCA. There is some distinction of the groups through this method but visualising only PC1 and PC2, the labeled groups were not fully distinguishable. 

![PCA](Data/figs/pca.png)

t-SNE performed much better than PCA for the separation of this data. This result is expected based on the normal performance of t-SNE on high dimensionality data when compared to PCA. This shows that in high-order space there is good separation of the categories. 

![t-SNE](Data/figs/t-sne.png)

To explore the need for dimensionality reduction for ML performance gains, I subset the features that provided 100, 99, 95, 90 and 85 of the variance of the data. Next, I ran a simple linear regression to see if there was an increase in performance as either a reduction in computational time or an increase in accuracy due to noise reduction. 


| Variance Retained        | Components           | Completion Time  | Accuracy        |
| ------------- |:-------------:| -----:|-------------|
| 1.0      | 300 | 0.007 | 95.1%        |
| .99      | 285      |   0.00003 | 95.3%        |
| .95 | 284      |    0.00002 | 95.3%        |
| .9 | 214      |  0.00005 | 94.8%        |
| .85  | 186      |    0.00001 | 94.6%        |

There is a small increase in the accuracy of the model and a two order of magnitude speed up for reducing the number of components (e^-3s v. e^-5). However, given the size of the data, I am concerned this may lead to over-fitting in some models so I will not make subsets at this point unless future models take a long time to train.

In order to make this data more usable, I transformed and filtered the data by removing all stop words as defined by `nltk` and removing all special characters, returns, new lines, empty spaces, punctuation and possessive articles. Next, I lemmatized the data. Lemmatization is the process of stemming words to reduce the inflectional forms and derivatives of a word to reduce them to a common base that expresses the same meaning. This is, in essence, a form of feature reduction that ensures that tokenization of the data doesn't return a different value for *Be* and *Is* when they are derivatives of the same verb. At this point, I calculated the Tf-IDF scores for each group, which is a metric for understanding the importance of a particular word to a group or label. Tf-IDF scores were used to calculate correlated unigrams to identify if there was overlap in the most descriptive words of each group, this helps to understand if there will be powerful features for classification with ML methods. 

| Category        | Unigrams1           | Unigram2  | Unigram3        | Unigram4        | Unigram5        |
| ------------- |:-------------:| -----:|-------------|-------------|-------------|
| Business      | market | economy | growth        | oil        | bank        |
| Entertainment     | music | best | star        | award        | film        |
| Politics      | tory | blair | election        | party       | labour        |
| Sports      | club | game | side        | team        | match        |
| Tech      | digital | computer | technology        | software        | users        |

The unigrams are non-overlapping! That's a nice result that there are resolvable features between the labels. Now the model building will start using the cleaned and filtered text strings saved for use with ML models. All of the machine learning was handled with scikitlearn because it is very well documented and the consistent API means training new models on the same data is trivial if the data is well prepared.

### ML Model performance 

The performance of each ML model was compared and tabulated below to highlight the consistently good performance of each method. With some hyperparameter tuning where applicable (not in the MNB approach), the results were mostly improved over the stock method. Performance of the models was measured solely by accuracy as the classes were relatively well balanced. A majority classifier would have had a performance of ~23% if it had called every article `sport`. If there had been greater imbalance in the category size AUC would have been a more appropriate metric.

#### Randomforest results

RF had a good out of the box performance with no hyperparameterization. It was ~89% on a training/test split of 80/20. Tuning the model improved the accuracy further up to 94.2% accuracy. 

#### SVM 

Support vector machines significantly underperformed out of the box with an accuracy of 22.9%, but improved massively with tuning to 95.05%. 

#### Multinomial Bayes

Bayesian methods have been successfully implemented in text classification before, so its not too surprising that the performance of this model was quite good at 94.4%.

#### Gradient Boosting Model

GBM also performed well out of the box at 93.5 %, strangely, optimisation increased the accuracy to 94.8%. An improvement over RF, the other ensemble learning methods. 

#### Neural Network Approach

I also tried a neural network approach using keras and tensorflow to see if the patterns in the high dimensional data could be picked up with a DL approach. The results were good, 95.5% and excluding the MNB method was by far the fastest to train.  

## Model evaluation 

All of these models performed comparably well, including the multi-class linear regression, which I performed only for benchmarking the performance gains of feature reduction. There is an improvement in the model accuracy from Rf < MNB < GBM < SVM < NN with a nearly commensurate increase in computational time with the exception of multinomial Bayes and the NN. MNB outperformed RF in speed and accuracy, second only in model build time to the keras/tensorflow implementation of a neural net which was the most accurate and the second fastest to compile. This could change with hyperparameter tuning of the NN model, but the out of box performance was superior to the ML methods regardless. These high accuracy results are possibly a reflection of the data that is being input, it would appear that the signal to noise ration is very high as a result of the preprocessing steps I took. 

| Model        | Testing accuracy  | 
| ------------- |:-------------:|
| RF      | 94.2% | 
| MNB     | 94.4% | 
| GBM      | 94.8% |
| SVM      | 95.05% |
| NN      | 95.5% |

## Conclusion 

Superficial features separating complex text problems are not sufficient for differentiating the labeled data and do not take into account any of the features of the data in their prediction. Visualisation of the data in higher dimensions reveals significant separation of the categories, but these do not translate to single features, but to combinations of features. Data processing and engineering is a relatively simple task because of handy libraries like `nltk` and the encoding functions in `sklearn` and `tensorflow`. The consistent API of `sklearn` makes testing additional algorithms simple and performance across the board is high, although training times for hyperparameter tuning on my laptop is fairly slow (up to 10 minutes). Keras provided the simplest and quickest model for solving the problem of predicting labeled data.